msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2022-12-29 18:31+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../light_table/lighttable_overview.rst:1
msgid "Overview to digiKam Light Table"
msgstr ""

#: ../../light_table/lighttable_overview.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, light table, synchronize, by-pair, compare, preview, canvas"
msgstr ""

#: ../../light_table/lighttable_overview.rst:14
msgid "Overview"
msgstr ""

#: ../../light_table/lighttable_overview.rst:16
msgid "Contents"
msgstr ""

#: ../../light_table/lighttable_overview.rst:22
msgid "The Light Table Comparing Side By Side Two RAW Files"
msgstr ""

#: ../../light_table/lighttable_overview.rst:24
msgid ""
"digiKam features a light table in a separate window to easily compare "
"images. It works with all supported image formats including RAW files."
msgstr ""

#: ../../light_table/lighttable_overview.rst:30
msgid ""
"The Main Icon-View Context Menu To Place Items To Compare On Light Table"
msgstr ""

#: ../../light_table/lighttable_overview.rst:32
msgid ""
"Select one or several images in any view from the main window, call **Place "
"onto Light Table** :kbd:`Ctrl+L` from the context menu. The selection will "
"be added to the light table, and its separate window will open. When you are "
"back to the digiKam main window you can quickly access the light table with :"
"menuselection:`Tools --> Light Table` :kbd:`L`."
msgstr ""

#: ../../light_table/lighttable_overview.rst:39
msgid "Screencast Of The digiKam Light Table Item Selection From the Thumbbar"
msgstr ""

#: ../../light_table/lighttable_overview.rst:42
msgid "Playing With The Stack Contents"
msgstr ""

#: ../../light_table/lighttable_overview.rst:45
msgid "Synchronized Mode"
msgstr ""

#: ../../light_table/lighttable_overview.rst:47
msgid ""
"From the thumbbar drag & drop images to the left and right comparison pane "
"below. A little arrow will indicate which copy is shown in which pane. The "
"current selected pane will be highlighted by frame. If you choose "
"**Synchronize** from the toolbar, any zoom and panning in one window will be "
"synchronously executed in the other pane, so that you can compare the same "
"areas of two images."
msgstr ""

#: ../../light_table/lighttable_overview.rst:54
msgid ""
"Screencast Of The digiKam Light Table Using The **Synchonize** Mode With "
"Side By Side Views"
msgstr ""

#: ../../light_table/lighttable_overview.rst:57
msgid "By Pair Mode"
msgstr ""

#: ../../light_table/lighttable_overview.rst:59
msgid ""
"Another mode is better suited for quickly culling from a series of images. "
"If you choose **By Pair** from the toolbar, the first two images will be "
"automatically inserted into the comparison panes. Click on any thumbnail to "
"make it the left side preview, the adjacent thumbnail to the right will be "
"inserted into the right pane. That make it easy to sift through a series of "
"similar images."
msgstr ""

#: ../../light_table/lighttable_overview.rst:66
msgid ""
"Screencast of the digiKam Light Table Using The **By-Pair** Mode With Side "
"By Side Views"
msgstr ""

#: ../../light_table/lighttable_overview.rst:68
msgid ""
"Of course, the usual edit actions work from the light table directly using :"
"menuselection:`File --> Edit...` :kbd:`F4`. This open current selected "
"preview canvas in **Image Editor**."
msgstr ""

#: ../../light_table/lighttable_overview.rst:70
msgid ""
"All image information from the main window right sidebar is available for "
"each of the two previews in the light table. The Light Table **Left "
"Sidebar** is dedicated to the **Left Pane**, and the **Right Sidebar** for "
"the **Right Pane**. This makes it easy to link visual differences to "
"exposure data for example."
msgstr ""

#: ../../light_table/lighttable_overview.rst:72
msgid ""
"In the lower right corner of each pane there you find a built-in panning "
"action (crossed arrows). Click on it and keep the left mouse button pressed "
"to pan across the image (with **Synchrone** mode, both images will show the "
"same viewing point)."
msgstr ""

#: ../../light_table/lighttable_overview.rst:74
msgid ""
"Zooming works the same as in other views: use indifferently the zoom slider "
"below the panes or **Ctrl-scroll wheel** to zoom in and out, with both "
"images when **Synchrone** mode is selected."
msgstr ""

#: ../../light_table/lighttable_overview.rst:78
msgid ""
"If you use muti-screen on your computer, it's a good idea to place **Main "
"Window** on a screen and the **Light Table** on other one to increase your "
"experience."
msgstr ""

#: ../../light_table/lighttable_overview.rst:82
msgid ""
"Light Table has a specific configuration page from digiKam setup dialog. For "
"more details, read :ref:`this section <lighttable_settings>` from the manual."
msgstr ""
