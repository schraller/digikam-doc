# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
# Valter Mura <valtermura@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.0.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-02-23 21:07+0100\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.2\n"

#: ../../asset_management/dam_workflow.rst:1
msgid "A Typical Digital Asset Management Workflow"
msgstr "Tipico flusso di lavoro di gestione dei beni digitali"

#: ../../asset_management/dam_workflow.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, digital, asset, management, workflow, export"
msgstr ""
"digiKam, documentazione, manuale utente, gestione fotografie, open source, "
"libero, apprendimento, facile, digitale, beni, gestione, flusso di lavoro, "
"esportazione"

#: ../../asset_management/dam_workflow.rst:14
msgid "DAM Workflow"
msgstr "Flusso di lavoro DAM"

#: ../../asset_management/dam_workflow.rst:16
msgid "Contents"
msgstr "Contenuto"

#: ../../asset_management/dam_workflow.rst:18
msgid ""
"Import images from camera, card reader or scanner. As long as the images are "
"stored on the camera media, you can use that as temporary backup."
msgstr ""
"Importa le immagini dalla fotocamera, dal lettore di schede o dallo scanner; "
"fino a quando queste sono conservate nella memoria della macchina "
"fotografica puoi usarle come una copia di sicurezza temporanea."

#: ../../asset_management/dam_workflow.rst:20
msgid "RAW are converted to DNG and stored away into an RAW archive."
msgstr "I file RAW sono convertiti in DNG e memorizzati in un archivio RAW."

#: ../../asset_management/dam_workflow.rst:26
msgid "The DNG Convert Settings From Import Tool"
msgstr "Le impostazioni di conversione DNG dallo strumento di importazione"

#: ../../asset_management/dam_workflow.rst:28
msgid "Rate and cull, write-back metadata to the DNG archive."
msgstr "Classifica e seleziona, e scrivi i metadati nell'archivio DNG."

#: ../../asset_management/dam_workflow.rst:34
msgid "Applying Labels to Items Using Captions Tab from Right Sidebar"
msgstr ""
"Applicare le etichette agli elementi utilizzando la scheda delle didascalie "
"dalla barra laterale destra"

#: ../../asset_management/dam_workflow.rst:36
msgid "Make a backup e.g. on external drive, and optical medium, or a tape."
msgstr "Fai una copia di sicurezza su DVD, su un CD o su un nastro magnetico."

#: ../../asset_management/dam_workflow.rst:38
msgid "Tag, comment, and geo-locate."
msgstr "Etichetta, commenta e geolocalizza."

#: ../../asset_management/dam_workflow.rst:44
msgid "Applying Descriptions to Items Using Captions Tab from Right Sidebar"
msgstr ""
"Applicare le descrizioni agli elementi utilizzando la scheda delle "
"didascalie dalla barra laterale destra"

#: ../../asset_management/dam_workflow.rst:46
msgid "Edit and improve photographs."
msgstr "Modifica e migliora le fotografie."

#: ../../asset_management/dam_workflow.rst:52
msgid "Sharpening Details In Image Editor Using Refocus Tool"
msgstr ""
"Migliorare i dettagli nell'Editor delle immagini utilizzando lo strumento "
"Rimessa a fuoco"

#: ../../asset_management/dam_workflow.rst:54
msgid ""
"For layered editing use external applications. Back in digiKam, re-apply the "
"metadata, which was probably lost or curtailed by the other applications."
msgstr ""
"Per modificare le immagini con il metodo dei livelli usa un'applicazione "
"esterna. Torna in digiKam e riapplica i metadati, dato che probabilmente "
"sono stati persi o parzialmente tagliati dall'altra applicazione."

#: ../../asset_management/dam_workflow.rst:56
msgid "Run the routine backup with following data-integrity checks."
msgstr ""
"Esegui le copie di sicurezza di routine nei successivi controlli di "
"integrità."

#: ../../asset_management/dam_workflow.rst:58
msgid ""
"Protect processed images for copyrights with Digital Watermarking. Export to "
"web galleries, slide shows, MPEG encode, contact sheets, printing, etc."
msgstr ""
"Proteggi i diritti d'autore delle immagini elaborate con la filigrana "
"digitale. Esporta nelle gallerie web, nelle presentazioni, nella codifica "
"MPEG, nelle schede dei contatti, nelle stampe, ecc."

#: ../../asset_management/dam_workflow.rst:64
msgid "The List of Export Tools Available From Image Editor"
msgstr ""
"Elenco degli strumenti di esportazione disponibili dall'Editor delle immagini"

#: ../../asset_management/dam_workflow.rst:66
msgid ""
"A typical generic draft of all photograph workflow stages is given below:"
msgstr ""
"Di seguito è riportata una tipica bozza di tutti i passaggi del flusso di "
"lavoro fotografico:"

#: ../../asset_management/dam_workflow.rst:72
msgid "Draft of all Common Tasks Used During a Photographs Workflow"
msgstr ""
"Bozza di tutte le operazioni comuni utilizzate durante un flusso di lavoro "
"fotografico"
