# Copyright (C) licensed under the  <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">licensed under the terms of the GNU Free Documentation License 1.2+</a> unless stated otherwise
# This file is distributed under the same license as the Digikam Manual package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Digikam Manual 8.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-08-29 13:15+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../maintenance_tools/maintenance_common.rst:1
msgid "digiKam Maintenance Tool Common Options"
msgstr "digiKam skupne možnosti orodja za vzdrževanje"

#: ../../maintenance_tools/maintenance_common.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, common options"
msgstr ""
"digiKam, dokumentacija, uporabniški priročnik, upravljanje fotografij, "
"odprta koda, prosto, učenje, enostavno, skupne možnosti"

#: ../../maintenance_tools/maintenance_common.rst:14
msgid "Common Options"
msgstr "Pogoste možnosti"

#: ../../maintenance_tools/maintenance_common.rst:16
msgid "Contents"
msgstr "Vsebina"

#: ../../maintenance_tools/maintenance_common.rst:22
msgid "The digiKam Maintenance Options to Select Albums"
msgstr "Možnosti vzdrževanja digiKam za izbiro albumov"

#: ../../maintenance_tools/maintenance_common.rst:24
msgid ""
"In the **Common Options** section you can restrict the scope of the "
"maintenance process(es) to certain albums or tags. You got to un-check "
"**Whole albums collection** or **Whole tags collection** to get access to "
"the drop down fields where you can select albums or tags. Note that in this "
"drop down field you have a context menu to select or deselect children and/"
"or parents and also to invert the selection. If you switch from **Albums** "
"to **Tags** or vice versa your selection in the now grayed out item will "
"remain untouched which might be useful if you need a certain selection more "
"often."
msgstr ""
"V razdelku **Pogoste možnosti** lahko omejite obseg postopkov vzdrževanja na "
"določene albume ali značke. Odkljukati morate **Celotna zbirka albumov** ali "
"**Celotna zbirka značk**, da dobite dostop do spustnih polj, kjer lahko "
"izberete albume ali značke. Upoštevajte, da imate v tem spustnem polju "
"kontekstni meni za izbiro ali preklic izbire otrok in/ali staršev ter tudi "
"za obračanje izbire. Če preklopite iz **Albumov** v **Značke** ali obratno, "
"bo vaš izbor v zdaj sivo obarvanem elementu ostal nedotaknjen, kar je lahko "
"koristno, če določen izbor potrebujete pogosteje."

#: ../../maintenance_tools/maintenance_common.rst:30
msgid "The digiKam Maintenance Options to Select Tags"
msgstr "Možnosti vzdrževanja digiKam za izbiro značk"

#: ../../maintenance_tools/maintenance_common.rst:32
msgid ""
"Since many of the processes the maintenance tool can perform are time "
"consuming (depending also on the scope, of course) you have the choice to "
"check **Work on all processor cores** if your machine has more than one and "
"they are not occupied otherwise."
msgstr ""
"Ker je veliko procesov, ki jih lahko izvaja vzdrževalno orodje, zamudnih "
"(seveda odvisno tudi od obsega), lahko izberete **Delo na vseh procesorskih "
"jedrih**, če ima vaš računalnik več kot eno jedro in niso zasedena z drugimi "
"opravili."

#: ../../maintenance_tools/maintenance_common.rst:38
msgid "The digiKam Maintenance Common Options to Process Tools"
msgstr "Pogoste možnosti vzdrževanja digiKam za orodja za obdelavo"

#: ../../maintenance_tools/maintenance_common.rst:40
msgid ""
"The **Use the Last Saved Active Tools and Settings** allows to force to "
"remember the options selection taken at last maintenance tool session."
msgstr ""
"**Uporabi zadnja shranjena aktivna orodja in nastavitve** omogoča prisilno "
"zapomnitev izbire možnosti v zadnji seji orodja za vzdrževanje."
