# Translation of docs_digikam_org_batch_queue___raw_converter.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-06-26 11:32+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../batch_queue/raw_converter.rst:1
msgid "digiKam RAW Converter from Batch Queue Manager"
msgstr "Convertidor RAW del gestor de la cua per lots del digiKam"

#: ../../batch_queue/raw_converter.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, batch, raw, converter"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, ajuda, aprenentatge, fàcil, lots, raw, convertidor"

#: ../../batch_queue/raw_converter.rst:14
msgid "RAW Converter"
msgstr "Convertidor RAW"

#: ../../batch_queue/raw_converter.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../batch_queue/raw_converter.rst:18
msgid ""
"With the Batch Queue Manager, you can import and convert your images stored "
"in RAW format to a demosaiced target formats as (JPEG, TIFF, or PNG)."
msgstr ""
"Amb el Gestor de la cua per lots, podreu importar i convertir les vostres "
"imatges emmagatzemades en format RAW a un format de reconstrucció com (JPEG, "
"TIFF, or PNG)."

#: ../../batch_queue/raw_converter.rst:20
msgid ""
"There are ome considerations before your convert, and even before you shoot "
"in RAW format. RAW image file formats keep the original information of the "
"exposure supposedly without loss (when compressed, they use reversible, "
"lossless compressions algorithms). Many photographers prefer to take their "
"picture in RAW mode because the camera's built-in conversion program is "
"often of lesser quality than what you can achieve with digital image "
"processing. The whitebalance of the digital camera is registered but not "
"applied to the image. Some CMOS CCD's create four types of color filter "
"data: RGBG, Red, Green, Blue and another Green mask for contrast "
"enhancement. This tool can take this into account."
msgstr ""
"Hi ha algunes consideracions abans de convertir, i fins i tot abans de "
"disparar en format RAW. Els formats de fitxer d'imatge RAW mantenen la "
"informació original de l'exposició suposadament sense pèrdua (quan es "
"comprimeix, s'utilitzen algorismes de compressió reversibles i sense "
"pèrdua). Molts fotògrafs prefereixen prendre la seva fotografia en mode RAW, "
"perquè el programa de conversió integrat en la càmera és de qualitat més "
"baixa que el que es podrà aconseguir amb el processament digital de la "
"imatge. El balanç de blancs de la càmera digital ha quedat registrat, però "
"no s'aplica a la imatge. Alguns CCD de CMOS creen quatre tipus de dades de "
"filtre de color: RGBG, Vermell, Verd, Blau i una altra màscara de Verd per a "
"millorar el contrast. Aquesta eina ho pot tenir en compte."

# skip-rule: t-acc_obe
#: ../../batch_queue/raw_converter.rst:22
msgid ""
"The conversion is based on `Libraw <https://www.libraw.org/>`_. Don't expect "
"to produce the same images as software provided by the camera vendor but "
"sometimes libraw gives better results. See :ref:`this section "
"<camera_profiles>` of this manual for details."
msgstr ""
"La conversió es basa realment en la `LibRaw <https://www.libraw.org/>`_. No "
"espereu produir les mateixes imatges que el programari proporcionat pel "
"venedor de la càmera, però a vegades la LibRaw dona millors resultats. Per a "
"obtenir més informació, vegeu :ref:`aquesta secció <camera_profiles>` del "
"manual."

#: ../../batch_queue/raw_converter.rst:24
msgid ""
"Select the RAW files to convert and load them to a dedicate batch queue."
msgstr ""
"Seleccioneu els fitxers RAW que s'han de convertir i carregueu-los a una cua "
"per lots dedicada."

#: ../../batch_queue/raw_converter.rst:26
msgid ""
"In Batch Queue Manager settings view, you can adjust the available check and "
"value boxes to optimize the RAW conversion process. The first checkbox "
"indicates to use the camera settings for white balance. The essential "
"parameters like brightness, and red/blue amplifier may be adjusted. If the "
"result is not quite optimal, don't worry, it is always possible to fine-tune "
"the image later on."
msgstr ""
"A la vista de les opcions de configuració del Gestor de la cua per lots, "
"podeu ajustar la marca disponible i els quadres de valors per a optimitzar "
"el procés de la conversió RAW. La primera casella de selecció indica "
"utilitzar la configuració de la càmera per al balanç de blancs. Es poden "
"ajustar els paràmetres essencials com la brillantor i un amplificador de "
"color vermell/blau. Si el resultat no és del tot òptim, no us preocupeu, "
"sempre serà possible afinar la imatge més endavant."

#: ../../batch_queue/raw_converter.rst:28
msgid ""
"The **Interpolate RGB as four colors** conversion option can be selected to "
"use contrast information. (If your camera works in RGB mode, the RGBG "
"setting has no effect). If you want to know all details about the **RAW "
"Decoding** settings see :ref:`this section <setup_rawdefault>` of this "
"manual."
msgstr ""
"Es pot seleccionar una opció de conversió **Interpola el RGB com a quatre "
"colors** per a utilitzar la informació de contrast. (Si la càmera funciona "
"en el mode RGB, l'opció de configuració **Descodificació RAW** no tindrà cap "
"efecte). Si voleu conèixer tots els detalls sobre la configuració de la "
"**Descodificació RAW**, vegeu :ref:`aquesta secció <setup_rawdefault>` del "
"manual."

#: ../../batch_queue/raw_converter.rst:30
msgid ""
"You must choose one output formats to save the conversion result by placing "
"the target file format conversion tool. The file name will stay the same by "
"default, only the extension changes. **JPEG** uses the lossy algorithm and "
"produces the smallest output file size. In opposite the tagged image format "
"(**TIFF**) preserves all information of your image while using lossless LZW "
"compression."
msgstr ""
"A l'eina de conversió haureu de triar un dels formats de sortida per a desar "
"el resultat de la conversió al format del fitxer de destinació. De manera "
"predeterminada, el nom del fitxer seguirà sent el mateix, només canviarà "
"l'extensió. El **JPEG** utilitza l'algorisme amb pèrdua i produeix una mida "
"del fitxer de sortida més petita. De forma oposada, el format d'imatge amb "
"etiquetes (**TIFF**) conservarà tota la informació de la vostra imatge "
"mentre utilitza la compressió LZW sense pèrdua."

#: ../../batch_queue/raw_converter.rst:34
msgid ""
"If you intend to work a lot on your images or if it is likely that you have "
"to reuse it later on, then don't use **JPEG** format because it allows a "
"limited number of operations before it deteriorates visibly. **TIFF** and "
"**PNG** are better suited to keep the original data."
msgstr ""
"Si intenteu treballar sobre unes quantes imatges, o si és probable que les "
"torneu a utilitzar més endavant, no utilitzeu el format **JPEG**, atès que "
"permet un nombre limitat d'operacions abans de deteriorar-les visiblement. "
"El **TIFF** i el **PNG** són més adequats per a mantenir les dades originals."

#: ../../batch_queue/raw_converter.rst:38
msgid ""
"If you choose **JPEG**, or **TIFF**, or **PNG** as the **Save Format** then "
"the metadata included in RAW file will be included in the target files as "
"Exif information."
msgstr ""
"Si escolliu **JPEG**, **TIFF** o **PNG** com el **Format de desament**, "
"llavors les ​​metadades incloses en el fitxer RAW s'inclouran en els fitxers "
"de destinació com a informació Exif."

#: ../../batch_queue/raw_converter.rst:40
msgid ""
"When you have finished to setup the queue, click the **Process** button to "
"start the conversion. If you want abort image conversion, press the "
"**Abort** button."
msgstr ""
"Quan finalitzeu de configurar la cua, feu clic en el botó **Executa** per a "
"iniciar la conversió de la imatge. Si la voleu interrompre, premeu el botó "
"**Interromp**."

#: ../../batch_queue/raw_converter.rst:46
msgid ""
"The Batch Queue Manager With a Workflow to Convert RAW files to HEIF "
"Container by Applying Filters"
msgstr ""
"El Gestor de la cua per lots amb un flux de treball per a convertir fitxers "
"RAW al contenidor HEIF aplicant filtres"
