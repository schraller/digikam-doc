# Translation of docs_digikam_org_post_processing___send_images.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://spdx.org/licenses/GFDL-1.2-or-later.html">terms of the GNU Free Documentation License 1.2</a> unless stated otherwise
# This file is distributed under the same license as the digiKam Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: digikam-doc\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-09 00:48+0000\n"
"PO-Revision-Date: 2023-02-08 11:36+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.1.1\n"

#: ../../post_processing/send_images.rst:1
msgid "The digiKam Tool to Send Images By Mail"
msgstr "L'eina per a enviar les imatges per correu electrònic en el digiKam"

#: ../../post_processing/send_images.rst:1
msgid ""
"digiKam, documentation, user manual, photo management, open source, free, "
"learn, easy, email, sharing"
msgstr ""
"digiKam, documentació, manual d'usuari, gestió de les fotografies, codi "
"obert, lliure, aprenentatge, fàcil, correu electrònic, compartir"

#: ../../post_processing/send_images.rst:14
msgid "Send Images"
msgstr "Enviar les imatges"

#: ../../post_processing/send_images.rst:16
msgid "Contents"
msgstr "Contingut"

#: ../../post_processing/send_images.rst:18
msgid "This tool can be used to send images directly by e-mail."
msgstr ""
"Aquesta eina es pot utilitzar per a enviar imatges directament per correu "
"electrònic."

#: ../../post_processing/send_images.rst:20
msgid ""
"With this tool you can e-mail the selected images with your favorite e-mail "
"agent. Resizing images is possible. You can use drag and drop to add more "
"images to the list of images to send."
msgstr ""
"Amb aquesta eina podreu enviar per correu electrònic les imatges "
"seleccionades amb el vostre agent de correu electrònic preferit. Podreu "
"canviar la mida de les imatges. Podreu utilitzar arrossega i deixa anar per "
"a afegir més imatges a la llista d'imatges que s'enviaran."

#: ../../post_processing/send_images.rst:22
msgid ""
"When opening the tool from the application, the **Email Images Options** "
"dialog will appear with the e-mail agent applications detected on your "
"system. You can also select the items to send by **Images** list or "
"**albums** collection."
msgstr ""
"Quan obriu l'eina des de l'aplicació, apareixerà el diàleg **Configuració "
"del correu electrònic** amb les aplicacions d'agent de correu electrònic "
"detectades en el vostre sistema. També podreu seleccionar els elements que "
"s'enviaran per a la llista **Imatges** o la col·lecció **Àlbums**."

#: ../../post_processing/send_images.rst:28
msgid "The Send Images E-mail Client Detection and Contents Selection"
msgstr ""
"La detecció del client de correu electrònic per a enviar les imatges i la "
"selecció del contingut"

#: ../../post_processing/send_images.rst:30
msgid ""
"With the Images list mode, you will show your pre-selected items from "
"digiKam already in the list of the dialog."
msgstr ""
"Amb el mode llista d'imatges, es mostraran els elements preseleccionats des "
"del digiKam que ja es troben a la llista del diàleg."

#: ../../post_processing/send_images.rst:36
msgid "The Send Images Tool and Selected List of Items"
msgstr "L'eina d'enviament d'imatges i la llista dels elements seleccionats"

#: ../../post_processing/send_images.rst:38
msgid ""
"Below the image list there are five buttons. The two left buttons are used "
"to move a selected image in the list up or down. The middle button opens a "
"file browser to add more files as needed in the usual file selection manner."
msgstr ""
"Sota la llista d'imatges hi ha cinc botons. Els dos botons de l'esquerra "
"s'utilitzen per a moure cap amunt o cap avall una imatge seleccionada a la "
"llista. El botó central obre un navegador de fitxers per a afegir-n'hi més "
"segons sigui necessari en la forma habitual de la selecció de fitxers."

#: ../../post_processing/send_images.rst:40
msgid ""
"When clicking on the other buttons the selected images will be removed from "
"the list or the list will be cleared."
msgstr ""
"Quan es fa clic en els altres botons, les imatges seleccionades s'eliminaran "
"de la llista o la llista serà netejada."

#: ../../post_processing/send_images.rst:44
msgid ""
"You can drag and drop images between the host application window and the "
"image file list to add new items to e-mail."
msgstr ""
"Podeu arrossegar i deixar anar imatges entre la finestra de l'aplicació pare "
"i la llista de fitxers d'imatge per a afegir elements nous al correu "
"electrònic."

#: ../../post_processing/send_images.rst:48
msgid ""
"Be sure not to overload your own or your addressees e-mail transmission "
"limit; most providers limit the e-mail size to some megabytes of data. Use "
"several e-mails if you want to send a lot of data."
msgstr ""
"Assegureu-vos de no sobrecarregar el compte o el vostre límit de transmissió "
"de destinataris de correu electrònic. La majoria dels proveïdors limiten la "
"mida dels correus a uns quants megabytes de dades. Utilitzeu diversos "
"correus electrònics si voleu enviar una gran quantitat de dades."

#: ../../post_processing/send_images.rst:50
msgid ""
"The next page is used to change the settings of the images to be sent. These "
"settings will be remembered when you close the tool until you change them."
msgstr ""
"La pàgina següent s'utilitza per a canviar les opcions de configuració de "
"les imatges que s'enviaran. Aquestes opcions de configuració es recordaran "
"quan tanqueu l'eina fins que les canvieu."

#: ../../post_processing/send_images.rst:52
msgid "Select your favorite **Mail Program** from the drop-down menu."
msgstr ""
"Seleccioneu el vostre **Programa de correu:** preferit des del menú "
"desplegable."

#: ../../post_processing/send_images.rst:54
msgid ""
"If you select **Attach a file with image properties from Application** the "
"comments, rating, or tags included with the images will be merged to a text "
"file and attached in the mail."
msgstr ""
"Si seleccioneu **Adjunta un fitxer amb les propietats de la imatge des de "
"l'«aplicació»**; els comentaris, la valoració o les etiquetes incloses amb "
"les imatges seran fusionades en un fitxer de text i adjuntat al correu."

#: ../../post_processing/send_images.rst:56
msgid ""
"The next three items allow you to set-up the image properties to be sent. To "
"enable these options check **Adjust image properties**. First select an "
"**Image size** suitable for your Internet connection speed and the target "
"mailbox size."
msgstr ""
"Els tres elements següents permeten establir les propietats de la imatge que "
"s'enviarà. Per a activar aquestes opcions, marqueu **Ajusta les propietats "
"de les imatges**. Primer, seleccioneu una **Mida de la imatge** adequada per "
"a la velocitat de la vostra connexió a Internet i la mida de la bústia de "
"destinació."

#: ../../post_processing/send_images.rst:58
msgid ""
"The **PNG** file format in the **Image format** drop-down menu is not well "
"adapted to reduce photographic images file size, but is best suited for "
"compressing lossless items like the TIFF file format. **PNG** must be used "
"if you use dial-up speed Internet and you want to preserve the photo "
"quality. Otherwise, **JPEG** is the recommended format."
msgstr ""
"El format de fitxer **PNG** en el menú desplegable **Format de la imatge:** "
"no està ben adaptat per a reduir la mida del fitxer de les imatges "
"fotogràfiques, però és més adequat per a comprimir elements sense pèrdua com "
"el format de fitxer TIFF. Haureu d'utilitzar **PNG** si utilitzeu Internet "
"de velocitat d'accés telefònic i voleu conservar la qualitat de la "
"fotografia. En cas contrari, el **JPEG** és el format recomanat."

#: ../../post_processing/send_images.rst:60
msgid ""
"The **Image quality** can be chosen with the input box. 75% is the standard "
"compression ratio for **JPEG** files. Setting compression to 100% will "
"result in lossless compression at the cost of the largest file size. The "
"difference in file size between 90% and 100% can be quite dramatic, whereas "
"the gain in quality is minor. Very good quality is already obtained with 85% "
"compression."
msgstr ""
"La **Qualitat de la imatge** es pot escollir amb el quadre d'entrada. El 75% "
"és la taxa de compressió estàndard per als fitxers **JPEG**. Ajustar la "
"compressió al 100% es traduirà en una compressió sense pèrdua amb un cost de "
"la mida de fitxer més gran. La diferència en mida del fitxer entre 90% i "
"100% pot ser força dramàtica, mentre que el guany en la qualitat és més "
"baixa. Una molt bona qualitat s'obté al 85% de compressió."

#: ../../post_processing/send_images.rst:64
msgid ""
"If you choose JPEG as the Image file format and the original files were in "
"JPEG format, the original Exif data included is preserved in e-mailed images."
msgstr ""
"Si escolliu **JPEG** com el **Format de fitxer d'imatge** i els fitxers "
"originals es trobaven en el format **JPEG**, les dades Exif originals "
"incloses es conservaran en les imatges dels correus electrònics."

#: ../../post_processing/send_images.rst:66
msgid "If you want to drop metadata, turn on **Remove all metadata** option."
msgstr ""
"Si voleu llençar les metadades, activeu l'opció **Elimina totes les "
"metadades**."

#: ../../post_processing/send_images.rst:72
msgid "The Send Images Tool Mail Program Selection and Attachment Properties"
msgstr ""
"L'eina d'enviament d'imatges amb la selecció del programa de correu i les "
"propietats dels adjunts"

#: ../../post_processing/send_images.rst:74
msgid ""
"The next page will show a progress dialog given a feedback on the progress "
"of your operation to prepare item before e-mailing. Press the **Cancel** "
"button during this stage if you want to abort the process."
msgstr ""
"La pàgina següent mostrarà un diàleg de progrés amb comentaris sobre el "
"progrés de la vostra operació per a preparar l'element abans d'enviar-lo per "
"correu electrònic. Premeu el botó **Cancel·la** durant aquesta etapa si "
"voleu interrompre el procés."

#: ../../post_processing/send_images.rst:76
msgid ""
"When all jobs are completed, this dialog will not close automatically and "
"you can consult the progress messages if any errors occurred during this "
"stage. The mail agent will be started automatically at the end."
msgstr ""
"Quan es completin totes les tasques, aquest diàleg no es tancarà "
"automàticament i podreu consultar els missatges de progrés per si s'ha "
"produït algun error durant aquesta etapa. L'agent de correu s'iniciarà "
"automàticament al final."

#: ../../post_processing/send_images.rst:82
msgid "The Send Images Tool Post Processing Items to Send By Mail"
msgstr ""
"L'eina d'enviament d'imatges postprocessant els elements que s'enviaran per "
"correu"

#: ../../post_processing/send_images.rst:84
msgid ""
"The composer window will be open with your favorite e-mail client. You just "
"need to add the addressee and the subject. The selected images will be "
"already be listed as attachments."
msgstr ""
"S'obrirà la finestra de l'editor amb el vostre client de correu electrònic "
"preferit. Només haureu d'afegir el destinatari i l'assumpte. Les imatges "
"seleccionades ja apareixeran com a fitxers adjunts."

#: ../../post_processing/send_images.rst:90
msgid "The Items Post Processed and Ready to Send with Thunderbird Mail Agent"
msgstr ""
"Els elements s'han processat i estan preparats per a enviar-los amb l'agent "
"de correu Thunderbird"

#: ../../post_processing/send_images.rst:92
msgid ""
"When email is sent, you can **Close** the progress dialog to purge all "
"temporary files."
msgstr ""
"Quan envieu un correu electrònic, podreu prémer el botó **Tanca** per a "
"tancar el diàleg de progrés i que es purguin tots els fitxers temporals."
